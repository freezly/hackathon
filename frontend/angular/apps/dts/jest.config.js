module.exports = {
  name: 'dts',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/dts',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
