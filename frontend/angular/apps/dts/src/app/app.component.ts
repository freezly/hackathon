import { Component, OnInit } from '@angular/core';
import { Exhibitor } from './result/result.component';
import { allexhibitors } from './data';
import { MessageService } from 'primeng/components/common/messageservice';

enum STATE {
  REGISTRATION,
  PREFERENCES,
  DATING,
  RESULT
}

@Component({
  selector: 'angular-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MessageService]
})
export class AppComponent implements OnInit {
  STATE = STATE;
  state: STATE = STATE.PREFERENCES;
  exhibitors: Exhibitor[] = Object.assign([], allexhibitors);
  filteredExhibitores: Exhibitor[];

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {}

  goToResult(exhibitors: Exhibitor[]) {
    this.exhibitors = exhibitors;
    this.state = STATE.RESULT;
    setTimeout(() => {
      console.log('show toast');
      this.messageService.add({
        severity: 'success',
        key: 'c',
        sticky: true,
        summary: 'Terminanfrage bestätigt',
        detail:
          'Frau Wild von Bioland würde sich gerne mit Ihnen heute um 12:30Uhr am Bioland Stand treffen'
      });
    }, 2000);
  }

  closeMessage() {
    this.messageService.clear();
  }

  filter(e: Exhibitor, cat: string[]) {
    for (const c of cat) {
      if (e.bild1Interessen !== null && e.bild1Interessen.indexOf(c) >= 0) {
        return true;
      }

      if (e.bild2Interessen !== null && e.bild2Interessen.indexOf(c) >= 0) {
        return true;
      }
    }

    return false;
  }

  goToDating(event: string[]) {
    this.filteredExhibitores = this.exhibitors.filter(e =>
      this.filter(e, event)
    );
    this.state = STATE.DATING;
  }
}
