import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, keyframes, animate, transition } from '@angular/animations';
import * as kf from '../keyframes';
import { Exhibitor } from '../result/result.component';

export enum LIKE {
  LIKE,
  NOTLIKE,
  SUPERLIKE
}
@Component({
  selector: 'angular-dating',
  templateUrl: './dating.component.html',
  styleUrls: ['./dating.component.css'],
  animations: [
    trigger('cardAnimator', [
      transition('* => wobble', animate(1000, keyframes(kf.wobble))),
      transition('* => swing', animate(1000, keyframes(kf.swing))),
      transition('* => jello', animate(1000, keyframes(kf.jello))),
      transition(
        '* => zoomOutRight',
        animate(1000, keyframes(kf.zoomOutRight))
      ),
      transition('* => slideOutLeft', animate(400, keyframes(kf.slideOutLeft))),
      transition(
        '* => slideOutRight',
        animate(400, keyframes(kf.slideOutRight))
      ),
      transition('* => slideOutTop', animate(400, keyframes(kf.slideOutTop))),
      transition(
        '* => rotateOutUpRight',
        animate(1000, keyframes(kf.rotateOutUpRight))
      ),
      transition('* => flipOutY', animate(1000, keyframes(kf.flipOutY)))
    ])
  ]
})
export class DatingComponent implements OnInit {
  showDialog: boolean;
  @Input() exhibitors: Exhibitor[];
  animationState: string;
  like = LIKE;
  @Output() endSwipe: EventEmitter<Exhibitor[]> = new EventEmitter();
  position = 0;
  constructor() {}

  ngOnInit() {}

  startAnimation(state) {
    console.log(state);
    if (!this.animationState) {
      this.animationState = state;
    }
  }

  resetAnimationState() {
    this.animationState = '';
  }

  setstatus(event: LIKE) {
    this.exhibitors[this.position].like = event;
    setTimeout(
      () => {
        if (event === LIKE.SUPERLIKE) {
          this.showDialog = true;
        } else {
          this.makeEndSwipe();
        }
      },
      event === LIKE.SUPERLIKE ? 900 : 300
    );

    console.log(this.exhibitors);
  }
  makeEndSwipe() {
    this.position++;
    if (this.position >= this.exhibitors.length) {
      this.endSwipe.emit(this.exhibitors);
      this.position--;
    }
  }

  hidePopup() {
    this.makeEndSwipe();
    this.showDialog = false;
  }
}
