import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { allexhibitors } from '../data';

@Component({
  selector: 'angular-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.css']
})
export class PreferencesComponent implements OnInit {
  preferences = [
    'Öl',
    'Wein',
    'Bier',
    'Getreideprodukte',
    'Wasser',
    'Fleisch',
    'Wurst',
    'Saft',
    'Bio'
  ];

  clickedItems = {};

  @Output() doneEdit: EventEmitter<string[]> = new EventEmitter();

  constructor() {}

  ngOnInit() {
    const all: string[] = [];
    let pref1 = allexhibitors.map(i => i.bild1Interessen);

    pref1.forEach(i => i.forEach(i2 => all.push(i2)));

    pref1 = allexhibitors.map(i => i.bild2Interessen);
    pref1.forEach(i => i.forEach(i2 => all.push(i2)));

    const distinctThings = all.filter(
      (thing, i, arr) => arr.findIndex(t => t === thing) === i
    );

    console.log('all prefs', distinctThings);
    this.preferences = distinctThings;
  }

  clicked(text) {
    console.log(text);
    this.clickedItems[text] =
      this.clickedItems[text] === undefined ? true : !this.clickedItems[text];
  }

  done() {
    const categories: string[] = [];
    // tslint:disable-next-line: forin
    for (const key in this.clickedItems) {
      const value = this.clickedItems[key];
      if (value === true) {
        categories.push(key);
      }
    }
    console.log(categories);
    this.doneEdit.emit(categories);
  }
}
