import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { LIKE } from '../dating/dating.component';

export interface Exhibitor {
  name: string;
  like: LIKE;
  hallPosition: string;
  pic: string;
  time: string;
  ansprechpartner: string;
  bild1?: string;
  bild1Interessen?: string[];
  bild2?: string;
  bild2Interessen?: string[];
}
@Component({
  selector: 'angular-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit, OnChanges {
  LIKE = LIKE;
  @Input() exhibitors: Exhibitor[];
  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    console.log(this.exhibitors);
  }

  filter(keyvaluelist: Exhibitor[]) {
    return keyvaluelist.filter(e => e.like !== LIKE.NOTLIKE);
  }
}
