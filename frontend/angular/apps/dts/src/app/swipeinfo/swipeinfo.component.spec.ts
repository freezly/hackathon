import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwipeinfoComponent } from './swipeinfo.component';

describe('SwipeinfoComponent', () => {
  let component: SwipeinfoComponent;
  let fixture: ComponentFixture<SwipeinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwipeinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwipeinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
